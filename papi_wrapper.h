#include <papi.h>

// we cant use directly the PAPI_VER_CURRENT in papi.h
// because rust-bindgen has a bug with expandind function like macros
const unsigned int _PAPI_LIBRARY_CURRENT_VERSION = PAPI_VER_CURRENT;
#undef PAPI_VER_CURRENT
const int PAPI_VER_CURRENT = (int)_PAPI_LIBRARY_CURRENT_VERSION;
