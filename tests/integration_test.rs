use operator_benchmarks::{Benchmark, Computation, MetricReporter};
use serde::Serialize;
use std::{iter::repeat, time::Duration};

use simplelog::{ColorChoice, Config, LevelFilter, TermLogger, TerminalMode};

struct Matmul {
    n: usize,
    a: Vec<f32>,
    b: Vec<f32>,
    c: Vec<f32>,
}
impl Matmul {
    fn new(n: usize) -> Self {
        let flat_len = n * n;
        Matmul {
            n,
            a: (0..flat_len).map(|i| i as f32).collect(),
            b: (0..flat_len).map(|i| i as f32).collect(),
            c: repeat(0.0).take(flat_len).collect(),
        }
    }
}

impl Computation for Matmul {
    fn name(&self) -> &str {
        "matmul"
    }
    fn execute(&mut self) {
        for i in 0..self.n {
            for j in 0..self.n {
                for k in 0..self.n {
                    self.c[i * self.n + j] = self.a[i * self.n + k] * self.b[k * self.n + j];
                }
            }
        }
    }
}

#[derive(Serialize)]
struct MetaDataMatmul {
    len_i: usize,
    len_j: usize,
    len_k: usize,
    data_type: &'static str,
    loop_order: &'static str,
}

struct DummyMetricReporter;

impl MetricReporter for DummyMetricReporter {
    fn report(&self, _counters_values: &[u64], _duration: Duration) -> f64 {
        42.0
    }

    fn name_with_unit(&self) -> &str {
        "Dummy"
    }
}

#[test]
fn test_main() {
    TermLogger::init(
        LevelFilter::Debug,
        Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )
    .unwrap();
    let matmul = Matmul::new(100);
    let metadata = MetaDataMatmul {
        len_i: 100,
        len_j: 100,
        len_k: 100,
        data_type: "float32",
        loop_order: "ijk",
    };

    let dummy_reporter = DummyMetricReporter {};
    let reporters: Vec<Box<dyn MetricReporter>> = vec![Box::new(dummy_reporter)];
    let mut bench = Benchmark::from_toml_file("config.toml").with_metrics_reporters(reporters);
    bench.run(matmul);
    let result = bench.result(metadata);
    result.show_summary();
}
