use super::{config::CachePolicy, runs::Run};
use crate::metrics::MetricReporter;
use crate::papi::EventsSet;
use crate::Computation;
use indicatif::{ProgressBar, ProgressDrawTarget, ProgressFinish, ProgressIterator, ProgressStyle};
use log::debug;
use log::warn;
use serde::Serialize;

#[derive(Serialize, Clone, Debug)]
pub struct Epoch {
    pub runs: Vec<Run>,
}

pub fn progress_bar(name: String, len: u64) -> ProgressBar {
    let pb = ProgressBar::with_draw_target(Some(len), ProgressDrawTarget::stdout())
        .with_finish(ProgressFinish::AndLeave);
    pb.set_style(
        ProgressStyle::with_template("[{elapsed}] [{bar:40.cyan/blue}] ({pos}/{len}, {prefix})")
            .unwrap()
            .progress_chars("##."),
    );
    pb.set_prefix(name);
    pb
}

impl Epoch {
    pub fn run<C: Computation>(
        events_set: &mut [EventsSet],
        mut computation: C,
        repetitions: usize,
        policy: CachePolicy,
        reporters: &[Box<dyn MetricReporter>],
        pb: ProgressBar,
    ) -> Self {
        if repetitions < 30 {
            warn!("{repetitions} repetitions is small (<30)",);
        }

        let mut result: Vec<Run> = Vec::new();
        for _ in (0..repetitions).progress_with(pb) {
            let splitted_counters_runs: Vec<Run> = events_set
                .iter_mut()
                .map(|counters| {
                    use CachePolicy::*;
                    match policy {
                        ColdCache => Run::measure_cold_cache(&mut computation, counters),
                        HotCache(nexecs) => {
                            Run::measure_hot_cache(&mut computation, counters, nexecs)
                        }
                    }
                })
                .collect();
            result.push(Run::merge(splitted_counters_runs, reporters));
        }
        debug!("Epoch ends {}", computation.name());
        Epoch { runs: result }
    }
}
