use colored::Colorize;
use serde::Serialize;

use std::fmt::Display;
/// A simple struct holding architectural information
/// about a machine. **All sizes are in number of Bytes.**
#[derive(Clone, PartialEq, Eq, Debug, Serialize)]
pub struct Machine {
    pub name: String,
    pub fma: u64,
    pub cores: u64,
    pub vector_size: u64,
    pub l1_cache_size: u64,
    pub l2_cache_size: u64,
    pub l3_cache_size: u64,
}

impl Machine {
    /// This function is mostly used for peak perf computation. It gives the theoretical minimum
    /// number of cycles used to perform a computation of n=`pb_fma` fused multiply add using
    /// floating point number of `sizeof_numeric` bytes with `threads` number of threads used.
    pub fn theoretical_cycles(&self, pb_fma: u64, sizeof_numeric: u64, threads: u64) -> f64 {
        if threads == 0 {
            panic!("Number of threads cant be zero!");
        }
        if sizeof_numeric == 0 {
            panic!("Size of numeric data type cant be zero");
        }
        let vector_size = self.vector_size / sizeof_numeric;
        let used_cores = threads.min(self.cores);
        (pb_fma as f64) / ((self.fma * used_cores * vector_size) as f64)
    }

    /// Get the number of elements than can fit in a vector using one particular data type.
    /// Since the intern `vector_size` is in number of bytes it's a straight forward divdivion by
    /// the size of the type in number of bytes.
    pub fn vector_size_in_f<T>() -> usize {
        let current_machine = Machine::default();
        current_machine.vector_size as usize / std::mem::size_of::<T>()
    }
}

impl Display for Machine {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(
            f,
            "{} machine using AVX {} with {} FMA ports on {} cores",
            self.name.purple(),
            self.vector_size * 8,
            self.fma,
            self.cores
        )
    }
}

mod buildinfo {
    include!(concat!(env!("OUT_DIR"), "/current_machine.rs"));
}
/// Default is the machine on which the program is compiled.
/// The info are fetched from a script from Christophe Guillon that do a quick measure of fma.
/// This script is executed in the build.rs script at compile time.
/// It needs to access perf_event etc so the usual command to disable paranoid and turbo boost are
/// needed here too.
impl Default for Machine {
    fn default() -> Self {
        buildinfo::machine()
    }
}

#[test]
fn peak_perf_cycles_max_thread() {
    let pb_size = 8 * 100 * 100 * 100;
    type F = f32;
    let float_size = std::mem::size_of::<F>() as u64;

    let machine = Machine::default();
    let pk_perf = machine.theoretical_cycles(pb_size, float_size, machine.cores);
    let pk_perf_more_threads = machine.theoretical_cycles(pb_size, float_size, machine.cores + 1);
    assert_eq!(pk_perf, pk_perf_more_threads)
}

#[test]
fn peak_perf_cycles_more_thread() {
    type F = f32;
    let float_size = std::mem::size_of::<F>() as u64;
    let machine = Machine::default();
    let pb_size = Machine::vector_size_in_f::<F>() as u64 * 1_000_000;
    let cycles_1_thread = machine.theoretical_cycles(pb_size, float_size, 1);
    let cycles_2_threads = machine.theoretical_cycles(pb_size, float_size, 2);
    assert!(cycles_1_thread > cycles_2_threads);
    assert_eq!(cycles_1_thread / cycles_2_threads, 2.0);
}

#[test]
#[should_panic(expected = "Number of threads cant be zero!")]
fn peak_perf_cycles_panic_thread() {
    let machine = Machine::default();
    machine.theoretical_cycles(8, 4, 0);
}
#[test]
#[should_panic(expected = "Size of numeric data type cant be zero")]
fn peak_perf_cycles_panic_data() {
    let machine = Machine::default();
    machine.theoretical_cycles(8, 0, 1);
}
#[test]
fn peak_perf_correct() {
    type F = f32;
    let float_size = std::mem::size_of::<F>() as u64;
    let machine = Machine::default();
    let vecsize = Machine::vector_size_in_f::<F>() as u64;
    let pb_size = machine.fma * vecsize * 100;
    let pk_perf = machine.theoretical_cycles(pb_size, float_size, 1);
    assert_eq!(pk_perf, 100.0)
}
