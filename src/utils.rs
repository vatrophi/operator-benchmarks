/// Apply a permutation to a vector.
///
/// # Arguments
///  - buf: the vector to shuffle.
///  - swap: array of index permutation such as swap\[old\_i\] = new\_i.
///
/// # Example
///
/// ```rust
/// use operator_benchmarks::utils::permutation;
/// let buf = vec![1,2,3,4];
/// let swap = vec![0,3,1,2];
/// assert_eq!(permutation(&buf, &swap), vec![1,3,4,2]);
/// ```
pub fn permutation<T: Copy>(buf: &Vec<T>, swap: &Vec<usize>) -> Vec<T> {
    assert_eq!(swap.len(), buf.len());
    let mut swapped = buf.clone();
    for (old_i, &new_i) in swap.iter().enumerate() {
        swapped[new_i] = buf[old_i];
    }
    swapped
}

pub enum IndicesOrdering {
    RowMajor,
    ColumnMajor,
}

impl IndicesOrdering {
    pub fn outter_index(&self, ndims: usize) -> usize {
        use IndicesOrdering::*;
        match self {
            ColumnMajor => ndims - 1,
            RowMajor => 0,
        }
    }

    pub fn inner_index(&self, ndims: usize) -> usize {
        use IndicesOrdering::*;
        match self {
            RowMajor => ndims - 1,
            ColumnMajor => 0,
        }
    }

    pub fn incr_nloops_idx(&self, indices: &mut Vec<usize>, sizes: &Vec<usize>) {
        assert_eq!(indices.len(), sizes.len());
        use IndicesOrdering::*;
        let ndims = indices.len();
        match self {
            RowMajor => {
                indices[ndims - 1] += 1;
                for i in (1..ndims).rev() {
                    // propagate the carry to next loop iteration counters
                    if indices[i] >= sizes[i] {
                        indices[i] = 0;
                        indices[i - 1] += 1;
                    }
                }
            }
            ColumnMajor => {
                indices[0] += 1;
                for i in 0..(ndims - 1) {
                    // propagate the carry to next loop iteration counters
                    if indices[i] >= sizes[i] {
                        indices[i] = 0;
                        indices[i + 1] += 1;
                    }
                }
            }
        }
    }
}
/// Map N indices in a ND array to the 1D index in the flattened array.
///
/// # Arguments
/// - indices: the N indices
/// - sizes: the max value (exclusive) for each index.
/// - order: RowMajor (indices outter to inner) or ColumnMajor (indices inner to outter).
///
/// # Example
/// ```rust
/// use operator_benchmarks::utils::{linearized_index, IndicesOrdering};
/// let sizes = vec![7,3];
/// let indices = vec![5,2];
/// let order = IndicesOrdering::RowMajor;
/// assert_eq!(linearized_index(&indices,&sizes,&order), 2 + 5 * 3);
/// ```
pub fn linearized_index(indices: &Vec<usize>, sizes: &[usize], order: &IndicesOrdering) -> usize {
    let mut res = 0;
    let mut stride = 1;
    let n = indices.len();
    match order {
        IndicesOrdering::ColumnMajor => {
            for i in 0..n {
                res += indices[i] * stride;
                stride *= sizes[i];
            }
        }
        IndicesOrdering::RowMajor => {
            for i in (0..n).rev() {
                res += indices[i] * stride;
                stride *= sizes[i];
            }
        }
    }
    res
}

/// Map 1 index of a flattened array to N indices of an ND array.
///
/// # Arguments
/// - index: The linearized index.
/// - sizes: the max value (exclusive) for each index.
/// - order: RowMajor (indices outter to inner) or ColumnMajor (indices inner to outter).
///
/// # Example
/// ```rust
/// use operator_benchmarks::utils::{indices_from_linearized, linearized_index, IndicesOrdering};
/// let sizes = vec![7,3];
/// let indices = vec![5,2];
/// let order = IndicesOrdering::RowMajor;
/// let index = linearized_index(&indices,&sizes,&order);
/// assert_eq!(index, 2 + 5 * 3);
/// assert_eq!(indices_from_linearized(index,&sizes,&order), indices);
/// ```
pub fn indices_from_linearized(
    index: usize,
    sizes: &Vec<usize>,
    order: &IndicesOrdering,
) -> Vec<usize> {
    let mut stride = 1;
    let mut indices = sizes.clone();
    let n = sizes.len();
    use IndicesOrdering::*;
    match order {
        ColumnMajor => {
            for dim in 0..n {
                indices[dim] = (index / stride) % sizes[dim];
                stride *= sizes[dim];
            }
        }
        RowMajor => {
            for dim in (0..n).rev() {
                indices[dim] = (index / stride) % sizes[dim];
                stride *= sizes[dim];
            }
        }
    }
    indices
}

/// Compute the generalised transposition for ND array.
///
/// # Arguments
/// - buf: vector to transpose
/// - swap: permutation of dimensions such as swap\[old\_i\] = new\_i.
/// - sizes: the sizes of dimensions.
/// - order: RowMajor (indices outter to inner) or ColumnMajor (indices inner to outter).
///
/// # Example
/// ```rust
/// use operator_benchmarks::utils::{reorder_indices, IndicesOrdering};
///
/// let buf = vec![0,1,2,3,4,5];
/// let sizes = vec![2,3];
/// let swap = vec![1,0];
/// let order = IndicesOrdering::RowMajor;
/// assert_eq!(reorder_indices(&buf,&swap,&sizes,&order), vec![0,3,1,4,2,5]);
/// ```
pub fn reorder_indices<T: Copy>(
    buf: &Vec<T>,
    swap: &Vec<usize>,
    sizes: &Vec<usize>,
    order: &IndicesOrdering,
) -> Vec<T> {
    assert_eq!(swap.len(), sizes.len());
    assert_eq!(buf.len(), sizes.iter().product::<usize>());
    let swapped_sizes = permutation(sizes, swap);
    let ndims = swap.len();
    let last = order.outter_index(ndims);
    let mut reordered = buf.clone();
    let mut indices: Vec<usize> = swap.iter().map(|_| 0).collect();
    while indices[last] < sizes[last] {
        let bufi = linearized_index(&indices, sizes, order);
        let rbufi = linearized_index(&permutation(&indices, swap), &swapped_sizes, order);
        reordered[rbufi] = buf[bufi];
        order.incr_nloops_idx(&mut indices, sizes);
    }
    reordered
}

#[test]
fn test_permutation() {
    let rotate_right = (0..10).map(|i| (i + 1) % 10).collect();
    let data = (0..10).collect();
    let res = permutation(&data, &rotate_right);
    let expected: Vec<usize> = (0..10)
        .map(|i| (i as isize - 1).rem_euclid(10) as usize)
        .collect();
    assert_eq!(res, expected);
}

#[test]
fn test_reorder() {
    let mut sizes = vec![4, 3, 2]; // here is a column major sizes order
                                   // since inner dim is the first one declared
    let mut swap = vec![1, 2, 0]; // after swap [2,4,3]
    let buf = vec![
        0, 3, 6, 9, // line end
        1, 4, 7, 10, // line end
        2, 5, 8, 11, // line end
        // face end
        12, 15, 18, 21, // line end
        13, 16, 19, 22, // line end
        14, 17, 20, 23, // line end
    ];
    let res = reorder_indices(&buf, &swap, &sizes, &IndicesOrdering::ColumnMajor);
    let expected = vec![
        0, 12, // line end
        3, 15, // line end
        6, 18, // line end
        9, 21, // line end
        // face end
        1, 13, // line end
        4, 16, // line end
        7, 19, // line end
        10, 22, // line end
        // face end
        2, 14, // line end
        5, 17, // line end
        8, 20, // line end
        11, 23, // line end
    ];
    assert_eq!(res, expected);
    // try the exact same stuff but in row major
    // so after swap is still [2,4,3] but 3 is the inner dimension
    swap.reverse();
    sizes.reverse();
    let res = reorder_indices(&buf, &swap, &sizes, &IndicesOrdering::RowMajor);
    let expected: Vec<i32> = (0..24).collect();
    assert_eq!(res, expected);
}
