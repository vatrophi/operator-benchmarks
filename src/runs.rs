use crate::papi::EventsSet;
use crate::Computation;
use crate::{metrics::MetricReporter, Machine};
use serde::Serialize;
use std::{
    hint::black_box,
    time::{Duration, SystemTime},
};

/// Struct to repeat execution of computation and measure duration and hardware counters.
/// # Measurement overhead
/// The duration measurement start before PAPI start and stops after PAPI stop.
/// It will be counting the overhead of these call and some conversion from c long long to u64.
/// This is because we prefer have more clean counters e.g number of cycles to compute peak perf.
#[derive(Serialize, Clone, Debug)]
pub struct Run {
    pub counters_values: Vec<u64>,
    pub duration: Duration,
    pub metrics_values: Vec<f64>,
}

fn clear_cache() -> i32 {
    let cur = Machine::default();
    let len = cur.l3_cache_size as usize / std::mem::size_of::<i32>();
    let buf: Vec<i32> = vec![0; len];
    let mut tmp = 0;
    for i in 0..len {
        tmp += buf[i];
    }
    tmp
}

impl Run {
    /// Function to measure in cold cache scenario.
    /// It clear the cache and do one execution.
    pub fn measure_cold_cache<C: Computation>(
        computation: &mut C,
        perf_counters: &mut EventsSet,
    ) -> Self {
        computation.inspect(1);
        // compiler barrier to clear cache without any compiler optimization
        black_box(clear_cache());
        // Note that the time measurement will count the overhead of papi start and stop.
        let start = SystemTime::now();
        perf_counters.start().unwrap();
        computation.execute();
        let counters_values = perf_counters.stop();
        let duration = start.elapsed();

        Run {
            counters_values: counters_values.unwrap().iter().map(|&x| x as u64).collect(),
            duration: duration.unwrap(),
            metrics_values: Vec::new(),
        }
    }

    /// Function to measure in hot cache scenario.
    /// It repeat the computation `nexecs` times and agglomerate the counters values as average.
    pub fn measure_hot_cache<C: Computation>(
        computation: &mut C,
        perf_counters: &mut EventsSet,
        nexecs: u64,
    ) -> Self {
        computation.inspect(nexecs);
        // Note that the time measurement will count the overhead of papi start and stop.
        let start = SystemTime::now();
        perf_counters.start().unwrap();
        for _ in 0..nexecs {
            computation.execute();
        }

        let counters_values = perf_counters.stop();
        let duration = start.elapsed();

        Run {
            counters_values: counters_values
                .unwrap()
                .iter()
                .map(|&x| (x as u64) / nexecs)
                .collect(),
            duration: duration.unwrap() / (nexecs as u32),
            metrics_values: Vec::new(),
        }
    }

    /// Merge n runs in one by extending its values since we can't measure more than 4 counters per
    /// run.
    pub fn merge(runs: Vec<Self>, reporters: &[Box<dyn MetricReporter>]) -> Self {
        let mut res = Run {
            duration: Duration::ZERO,
            counters_values: Vec::new(),
            metrics_values: Vec::new(),
        };
        let n = runs.len();
        for run in runs {
            res.counters_values.extend(run.counters_values);
            res.duration += run.duration;
        }
        res.duration /= n as u32;
        res.metrics_values = reporters
            .iter()
            .map(|r| r.report(&res.counters_values, res.duration))
            .collect();
        res
    }
}
