use std::fmt::{Debug, Display};

use serde::Serialize;

/// Struct that summarize data.
#[derive(Debug, Serialize, Clone)]
pub struct Stats {
    /// Length of the sample.
    pub n: f64,
    /// Average value of the sample.
    pub average: f64,
    /// Standard deviation or sqrt(variance) of the sample.
    pub std_dev: f64,
}

impl Display for Stats {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let interval = 1.96 * self.std_dev / (self.n - 1.0).sqrt();
        let avg = self.average;
        if avg < 1000.0 {
            write!(f, "{avg:.2} ± {interval:.2}")
        } else {
            write!(f, "{avg:.2e} ± {interval:.2e}")
        }
    }
}

impl Stats {
    /// Average data from float sample.
    pub fn new(data: Vec<f64>) -> Self {
        if data.is_empty() {
            panic!("Cannot do stats with zero data!");
        }
        let n = data.len() as f64;
        let avg = data.iter().sum::<f64>() / n;
        let var = data.iter().map(|d| (d - avg) * (d - avg)).sum::<f64>() / n;
        Stats {
            n,
            average: avg,
            std_dev: var.sqrt(),
        }
    }
}

#[test]
#[should_panic(expected = "Cannot do stats with zero data!")]
fn test_panic_stats_no_data() {
    let empty: Vec<f64> = Vec::new();
    Stats::new(empty);
}
