use crate::config::Config;
use crate::epoch::Epoch;
use crate::machine::Machine;
use crate::stats::Stats;
use serde::Serialize;
use std::collections::HashMap;
use std::fmt::Display;

/// The generic struct for handling result of a [Benchmark](crate::Benchmark).
/// It can be parametrized by a metadata type `M` that can be serialized using [serde].
#[derive(Serialize)]
pub struct BenchResult<M: Serialize> {
    /// Copy of the configuration given to [Benchmark::new](crate::Benchmark::new)
    pub benchmark_config: Config,
    pub machine: Machine,
    pub metrics: Vec<String>,
    pub metadata: M,
    /// Here computations names are the key and the measure data are the values.
    pub data: HashMap<String, Epoch>,
}

/// Internal data used to gather measurement in [Stats].
#[derive(Serialize, Debug, Clone)]
pub struct EpochSummary {
    pub execution_time: Stats,
    pub other_metrics: Vec<Stats>,
}

/// Compacted data to one or two numbers to easily show a table as
/// summary.
#[derive(Serialize, Debug, Clone)]
pub struct BenchSummary<'a> {
    pub columns_names: Vec<String>,
    pub condensed: HashMap<&'a str, EpochSummary>,
}

impl Display for BenchSummary<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Compute width of each column
        let margin = 2;
        let mut column_widths: Vec<usize> = self
            .columns_names
            .iter()
            .map(|n| n.len() + margin)
            .collect();
        for (name, stats) in self.condensed.iter() {
            column_widths[0] = column_widths[0].max(name.len() + margin);
            column_widths[1] =
                column_widths[1].max(stats.execution_time.to_string().len() + margin);
            for (i, metric) in stats.other_metrics.iter().enumerate() {
                column_widths[i + 2] = column_widths[i + 2].max(metric.to_string().len() + margin);
            }
        }

        // write array header
        for (col_name, &width) in self.columns_names.iter().zip(column_widths.iter()) {
            write!(f, "{col_name:^width$}")?;
        }
        writeln!(f)?;
        writeln!(f, "{:-<len$}", "", len = column_widths.iter().sum())?;

        // write array line by line with its metrics values
        for (name, stats) in self.condensed.iter() {
            // adding color modify the len so the align fmt are broken
            // let dif = colored_perf.len() - avg_perf.len() - var_perf.len();
            // let width = width + dif;
            write!(
                f,
                "{name:^width0$}{:^width1$}",
                format!("{}", stats.execution_time),
                width0 = column_widths[0],
                width1 = column_widths[1],
            )?;

            let mut i = 2;

            for col_value in stats.other_metrics.iter() {
                write!(
                    f,
                    "{:^width$}",
                    format!("{}", col_value),
                    width = column_widths[i]
                )?;
                i += 1;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}
impl<M: Serialize> BenchResult<M> {
    /// Create a [BenchSummary] struct to compact all measurement in one or two numbers [Benchmark](crate::Benchmark).
    pub fn get_summary(&self) -> BenchSummary {
        let condensed: HashMap<&str, EpochSummary> = self
            .data
            .iter()
            .map(|(name, epoch)| {
                let times: Vec<f64> = epoch
                    .runs
                    .iter()
                    .map(|r| r.duration.as_secs_f64() * 1000.0)
                    .collect();
                let metrics: Vec<Stats> = (0..(self.metrics.len()))
                    .map(|i| {
                        let data: Vec<f64> =
                            epoch.runs.iter().map(|run| run.metrics_values[i]).collect();
                        Stats::new(data)
                    })
                    .collect();
                let summary = EpochSummary {
                    execution_time: Stats::new(times),
                    other_metrics: metrics,
                };
                (name.as_str(), summary)
            })
            .collect();
        let mut columns_names = vec!["Name".to_owned(), "ExecutionTime (ms)".to_owned()];
        columns_names.extend(self.metrics.clone());
        BenchSummary {
            condensed,
            columns_names,
        }
    }

    /// Create a [BenchSummary] struct and display it as a summary of the [Benchmark](crate::Benchmark).
    pub fn show_summary(&self) {
        println!("{}", self.benchmark_config);
        println!("{}", self.machine);
        let meta = serde_json::to_string_pretty(&self.metadata).unwrap();
        println!("{meta}");
        println!("{}", self.get_summary())
    }
}
