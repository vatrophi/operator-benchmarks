mod benchmark;
mod benchresult;
pub mod config;
mod epoch;
mod machine;
mod metrics;
pub mod papi;
mod runs;
mod stats;
pub mod utils;

pub use benchmark::Benchmark;
pub use benchresult::BenchResult;
pub use benchresult::BenchSummary;
pub use benchresult::EpochSummary;
pub use config::Config;
pub use machine::Machine;
pub use metrics::{GflopsReporter, MetricReporter, PeakPerfReporter, UsefulGFlops};
pub use stats::Stats;

/// This is the trait to implement to be runned in a Benchmark.
pub trait Computation {
    /// Name of the computation to identify the perf results
    /// for output to stdout or any serialized format.
    fn name(&self) -> &str;

    /// This function will be repeated a lot.
    /// Use this to call an optimized extern C function for example.
    fn execute(&mut self);

    /// This function is called before the repeated execute() calls.
    /// Its purpose is to allow inspector executor pattern used by some library like MKL.
    fn inspect(&mut self, _nexecs: u64) {}
}
