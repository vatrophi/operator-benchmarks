use crate::epoch::{progress_bar, Epoch};
use crate::papi::{papi_init, papi_shutdown, EventsSet};
use crate::{BenchResult, Computation, Config, Machine, MetricReporter};
use colored::Colorize;
use indicatif::ProgressBar;
use log::{error, warn};
use serde::Serialize;
use std::collections::HashMap;
use std::fs::read_to_string;

/// Main struct of this library, it can run any struct that implement [Computation]
/// and return a [BenchResult]. This result can be used to get a summary or to be saved as json or
/// any format using [serde].
pub struct Benchmark {
    config: Config,
    enable_progress_bar: bool,
    epochs: HashMap<String, Epoch>,
    reporters: Vec<Box<dyn MetricReporter>>,
    events: Vec<EventsSet>,
}

/// A carefully written drop method to drop all [EventsSet] before calling [papi_shutdown] to avoid
/// any manual error with PAPI and its file descriptors.
impl Drop for Benchmark {
    fn drop(&mut self) {
        self.events.clear();
        papi_shutdown();
    }
}

impl Benchmark {
    /// Create a new benchmark from parameters given in a config_file.toml.
    pub fn from_toml_file(path: &str) -> Self {
        match std::fs::read_to_string(path) {
            Ok(content) => {
                let conf: Config = toml::from_str(&content).expect("Invalid toml.");
                Self::new(conf)
            }
            Err(e) => {
                error!("Cannot read file {path} not found!");
                panic!("{e:?}");
            }
        }
    }

    /// Extract PAPI counters names from the config.
    pub fn counters_names(&self) -> Vec<&str> {
        self.config
            .epochs
            .papi_counters
            .iter()
            .map(String::as_str)
            .collect()
    }

    /// Add the metrics to collect during measurement.
    pub fn with_metrics_reporters(mut self, reporters: Vec<Box<dyn MetricReporter>>) -> Self {
        self.reporters = reporters;
        self
    }

    /// Disable the progress bar.
    pub fn without_progress(mut self) -> Self {
        self.enable_progress_bar = false;
        self
    }

    /// Main entry point. It calls [papi_init] and check some system config like turbo boost and
    /// perf event paranoid before trying to execute computations.
    pub fn new(config: Config) -> Self {
        papi_init().unwrap();
        if (!config.epochs.turbo_boost) && (!is_turbo_disabled()) {
            let cmd = format!("sudo sh -c 'echo 1 > {NO_TURBO_PATH}'");
            error!("To disable turbo boost try: {}", cmd.bright_yellow());
            panic!("Turbo boost is enabled");
        }

        if !is_paranoid_disabled() {
            let cmd = format!("sudo sh -c 'echo 1 > {NO_PARANO_PATH}'");
            error!(
                "To disable perf event paranoid try: {}",
                cmd.bright_yellow()
            );
            panic!("Perf event paranoid is enabled");
        }
        Benchmark {
            events: config.events_sets(),
            enable_progress_bar: true,
            config,
            epochs: HashMap::new(),
            reporters: Vec::new(),
        }
    }

    /// Execute an epoch of run on a given computation.
    /// # Warning
    /// This function will be long.
    pub fn run<C: Computation>(&mut self, computation: C) -> &mut Self {
        let name = computation.name().to_owned();
        let repetitions = self.config.epochs.repetitions;
        let progress = if self.enable_progress_bar {
            progress_bar(name.clone(), repetitions as u64)
        } else {
            ProgressBar::hidden()
        };
        let epoc = Epoch::run(
            &mut self.events,
            computation,
            repetitions,
            self.config.epochs.cache_policy,
            &self.reporters,
            progress,
        );
        let already_in_table = self.epochs.insert(name.clone(), epoc);
        if already_in_table.is_some() {
            warn!("Key {name} already in table, Value has been overwritten!",);
        }
        self
    }

    /// Extract all the measurement and config from [Benchmark] and combine with some external
    /// metadata to create a serializable [BenchResult] that can be written to json with [serde].
    pub fn result<M: Serialize>(self, metadata: M) -> BenchResult<M> {
        BenchResult {
            benchmark_config: self.config.clone(),
            machine: Machine::default(),
            metrics: self
                .reporters
                .iter()
                .map(|r| r.name_with_unit().to_owned())
                .collect(),
            metadata,
            data: self.epochs.clone(),
        }
    }
}

const NO_TURBO_PATH: &str = "/sys/devices/system/cpu/intel_pstate/no_turbo";
const NO_PARANO_PATH: &str = "/proc/sys/kernel/perf_event_paranoid";
const NO_PARANO_LIMIT: i32 = 2;

fn is_turbo_disabled() -> bool {
    read_to_string(NO_TURBO_PATH).unwrap().trim() == "1"
}
fn is_paranoid_disabled() -> bool {
    read_to_string(NO_PARANO_PATH)
        .unwrap()
        .trim()
        .parse::<i32>()
        .unwrap()
        < NO_PARANO_LIMIT
}
