use crate::papi::EventsSet;
use log::debug;
use serde::{Deserialize, Serialize};
use std::{fmt::Display, process::Command};

/// Enum to select the strategy with PAPI counters (try as many counters as possible per execution
/// or just 1 execution per counter)
#[derive(Deserialize, Serialize, Clone, Debug, Default, PartialEq, Eq)]
pub enum PapiPolicy {
    Accuracy,

    #[default]
    Speed,
}

impl PapiPolicy {
    /// Number of counters to group 1 for Accuracy and 4 for Speed.
    pub fn max_counters_together(&self) -> usize {
        match self {
            PapiPolicy::Accuracy => 1,
            PapiPolicy::Speed => 4,
        }
    }
}

/// Enum to select benchmark approach for repeating executions should we clear the cache or just
/// try to repeat the computation to stay with useful data in cache.
#[derive(Clone, Copy, Deserialize, Serialize, Debug, PartialEq, Eq)]
pub enum CachePolicy {
    ColdCache,
    HotCache(u64),
}

impl Default for CachePolicy {
    fn default() -> Self {
        CachePolicy::HotCache(10)
    }
}

impl Display for CachePolicy {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use CachePolicy::*;
        match self {
            ColdCache => write!(f, "Cold cache (1 execution only)"),
            HotCache(ex) => write!(f, "Hot cache ({ex} executions)"),
        }
    }
}

fn default_repetitions() -> usize {
    30
}

fn default_counters() -> Vec<String> {
    vec!["CPU_CLK_UNHALTED".to_owned(), "PAPI_SP_OPS".to_owned()]
}

#[derive(Deserialize, Serialize, Clone, Debug, PartialEq, Eq)]
pub struct EpochsInfo {
    #[serde(default = "default_repetitions")]
    pub repetitions: usize,

    /// Are we ok with turbo boost on ?
    /// true mean it's ok if turbo_boost is on
    /// false mean an error will occur if turbo is on
    #[serde(default)]
    pub turbo_boost: bool,

    #[serde(default)]
    pub cache_policy: CachePolicy,

    #[serde(default)]
    pub papi_policy: PapiPolicy,

    #[serde(default = "default_counters")]
    pub papi_counters: Vec<String>,
}

impl Default for EpochsInfo {
    fn default() -> Self {
        EpochsInfo {
            repetitions: default_repetitions(),
            turbo_boost: false,
            cache_policy: CachePolicy::default(),
            papi_policy: PapiPolicy::default(),
            papi_counters: default_counters(),
        }
    }
}

fn default_bench_name() -> String {
    "TestBenchmark".to_owned()
}

#[derive(Deserialize, Serialize, Clone, Debug, PartialEq, Eq)]
pub struct BenchInfo {
    #[serde(default = "default_bench_name")]
    pub name: String,

    #[serde(default)]
    pub description: String,

    #[serde(default = "today")]
    pub date: String,

    #[serde(default = "default_author")]
    pub author: String,

    #[serde(default = "current_commit")]
    pub commit: String,

    #[serde(default = "current_dirty_files")]
    pub dirty: String,
}

impl Default for BenchInfo {
    fn default() -> Self {
        BenchInfo {
            name: default_bench_name(),
            description: String::default(),
            date: today(),
            author: default_author(),
            commit: current_commit(),
            dirty: current_dirty_files(),
        }
    }
}

/// A generic helper to lauch a command in a subprocess wait and return its stdout as [String].
pub fn output_of_command(command: &'static str, arguments: &[&'static str]) -> String {
    let err_msg = format!("{} failed", command);
    let output = Command::new(command)
        .args(arguments)
        .output()
        .expect(&err_msg)
        .stdout;
    String::from_utf8(output).unwrap().trim().to_owned()
}

fn default_author() -> String {
    output_of_command("git", &["config", "user.name"])
}
fn today() -> String {
    output_of_command("date", &[])
}

fn current_commit() -> String {
    output_of_command("git", &["rev-parse", "HEAD"])
}

fn current_dirty_files() -> String {
    output_of_command("git", &["status", "--short"])
}

/// The configuration of the benchmark containing some metadata about the benchmark,
/// and some experimental configuration like turbo_boost, number of repetitions etc.
/// It can be built from toml or json file using [serde] to deserialize.
/// # Defaut values
/// - `benchmark.name` is `"TestBenchmark"`
/// - `benchmark.author` is the hostname
/// - `benchmark.date` is today
/// - `benchmark.commit` is the hash of the current commit
/// - `benchmark.dirty` is a summary of `git status`
/// - `epochs.repetitions` is 30
/// - `epochs.cache_policy` is `CachePolicy::HotCache(10)`
/// - `epochs.papi_policy` is [PapiPolicy::Speed]
/// - `epochs.turbo_boost` is `false` i.e turbo disabled.
/// - `epochs.papi_counters` are  CPU_CLK_UNHALTED and PAPI_SP_OPS.
#[derive(Deserialize, Serialize, Clone, Debug, Default, PartialEq, Eq)]
pub struct Config {
    /// Metadata of the benchmark. Most of the info are filled automatically but there are some
    /// fields that should be filled by human such as name and description.
    #[serde(default)]
    pub benchmark: BenchInfo,

    /// Details about how to benchmark.
    #[serde(default)]
    pub epochs: EpochsInfo,
}

impl Display for Config {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Repetitions: {}", self.epochs.repetitions)?;
        let per_exec = self.epochs.papi_policy.max_counters_together();
        writeln!(f, "Papi counters policy: {per_exec} per exec")?;
        writeln!(f, "Cache policy: {}", self.epochs.cache_policy)
    }
}

impl Config {
    /// Convert the PAPI counter stored as [String] to [EventsSet] for measuring purposes.
    /// # Notes
    /// The output depend on the value of `epochs.papi_counters`:
    /// - [PapiPolicy::Accuracy]: we do 1 counter per execution.
    /// - [PapiPolicy::Speed]: we try to gather as many counters as possible per execution. It results in a
    /// smaller vec than `epochs.papi_counters`.
    pub fn events_sets(&self) -> Vec<EventsSet> {
        let n_counters = self.epochs.papi_counters.len();
        let max_per_exec = self.epochs.papi_policy.max_counters_together();
        let mut events: Vec<EventsSet> = Vec::new();
        let mut n_scheduled = 0;

        // try to measure as many counters per run as possible
        // if we cant go for 4 counters we try 3,2,1
        while n_scheduled < n_counters {
            let max_per_exec = max_per_exec.min(n_counters - n_scheduled);
            let event_set = (1..=max_per_exec)
                .rev()
                .find_map(|n| {
                    EventsSet::new(&self.epochs.papi_counters[n_scheduled..(n_scheduled + n)]).ok()
                })
                .expect("No papi event can be scheduled (should never happen)");
            n_scheduled += event_set.len();
            events.push(event_set);
        }
        debug!("nexecs for PAPI counters {n}", n = events.len());
        events
    }
}

#[test]
fn test_cmd() {
    let output = output_of_command("ls", &[]);
    assert!(output.contains("Cargo.toml"));
}
