#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use log::debug;
use std::{
    ffi::{c_int, c_longlong, CStr, CString},
    fmt::Debug,
};

// generated during compilation by bindgen look build.rs for details
mod bindings {
    include!(concat!(env!("OUT_DIR"), "/papi_bindings.rs"));
}

use bindings::*;

#[derive(PartialEq, Eq, Clone, Copy)]
pub struct PapiError(i32);

const PAPI_OK_i: i32 = PAPI_OK as i32;

fn papi_check_ret(code: i32) -> Result<(), PapiError> {
    if code == PAPI_OK_i {
        Ok(())
    } else {
        Err(PapiError(code))
    }
}
/// Safe wrapper of the C function `PAPI_library_init` called with the current version of the PAPI
/// library.
/// # Warning
/// It should be call once at the creation of the `Benchmark` and never again before the call to
/// [papi_shutdown].
pub fn papi_init() -> Result<(), PapiError> {
    debug!("PAPI VERSION = {PAPI_VER_CURRENT}");
    let ret = unsafe { PAPI_library_init(PAPI_VER_CURRENT) };
    if ret == PAPI_VER_CURRENT {
        debug!("PAPI init ok");
        Ok(())
    } else {
        papi_check_ret(ret)
    }
}

/// Safe wrapper of the C function `PAPI_shutdown`.
/// # Warning
/// It should be called once only after [papi_init] and all the `EventsSet` are no longer in use if
/// not Segfault will probably occur.
/// Under the hood the function close all the file descriptors to the kernel perf event so forget
/// to call this can result in a "Too many files open" error at runtime.
pub fn papi_shutdown() {
    unsafe { PAPI_shutdown() }
}

fn papi_event_code_from_name(name: CString) -> Result<c_int, PapiError> {
    let mut code = 0;
    let ret = unsafe { PAPI_event_name_to_code(name.as_ptr(), &mut code) };
    papi_check_ret(ret).map(|_| code)
}

impl Debug for PapiError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let err_msg = unsafe { CStr::from_ptr(PAPI_strerror(self.0 as c_int)) };
        write!(f, "PapiError {code} i.e {err_msg:?}", code = self.0)
    }
}

/// Main struct for hardware counters measurement.
/// # Warning
/// This should be used only between [papi_init] and [papi_shutdown] calls!
/// # Example
/// ```rust
/// use operator_benchmarks::papi::*;
/// fn fib(n:usize) -> usize {
///     if n <= 1 {
///         1
///     } else {
///         fib(n-1) + fib(n-2)
///     }
/// }
///
/// fn main() {
///     let counters = ["PAPI_TOT_CYC".to_string()];
///     papi_init();
///     let mut events_sets = EventsSet::new(&counters).unwrap();
///     events_sets.start().unwrap();
///     let res = fib(12);
///     let counter_values = events_sets.stop().unwrap();
///     println!("fib(12)={res}, CYCLES={}",counter_values[0]);
///     drop(events_sets); // if not used here Segfault
///     papi_shutdown();
/// }
/// ```
pub struct EventsSet {
    counters_codes: Vec<c_int>,
    counter_values: Vec<c_longlong>,
    evt_set_id: c_int,
}

impl EventsSet {
    /// Return the number of counters in the `EventsSet`.
    pub fn len(&self) -> usize {
        self.counters_codes.len()
    }

    /// Create `EventsSet` from the counter names.
    /// # Error
    /// Error could occur if you try too much counters at once usually more than 4 will fail.
    /// This number depends on the counters some conflict each others so it could be lower than 4.
    /// This is why [events_sets](crate::Config::events_sets) is more complicated than just
    /// grouping counters 4 by 4.
    pub fn new(counters: &[String]) -> Result<Self, PapiError> {
        let counters_codes: Result<Vec<c_int>, PapiError> = counters
            .iter()
            .map(|name| {
                let name = CString::new(name.as_str()).unwrap();
                papi_event_code_from_name(name)
            })
            .collect();
        let counters_codes = counters_codes?;

        // adding events to PAPI
        let mut evt_set_id = PAPI_NULL;
        let ret = unsafe { PAPI_create_eventset(&mut evt_set_id) };
        papi_check_ret(ret)?;
        counters_codes.iter().try_for_each(|&code| {
            let ret = unsafe { PAPI_add_event(evt_set_id, code) };
            papi_check_ret(ret)
        })?;

        Ok(Self {
            counters_codes,
            counter_values: vec![0; counters.len()],
            evt_set_id,
        })
    }

    /// Safe wrapper arround `PAPI_start`.
    /// # Warning
    /// This should be used only between [papi_init] and [papi_shutdown] calls!
    pub fn start(&mut self) -> Result<(), PapiError> {
        let ret = unsafe { PAPI_start(self.evt_set_id) };
        papi_check_ret(ret)
    }

    /// Safe wrapper arround `PAPI_stop`.
    /// # Warning
    /// This should be used only between [papi_init] and [papi_shutdown] calls!
    pub fn stop(&mut self) -> Result<&[c_longlong], PapiError> {
        let ret = unsafe { PAPI_stop(self.evt_set_id, self.counter_values.as_mut_ptr()) };
        papi_check_ret(ret).map(|_| self.counter_values.as_slice())
    }
}

/// Integration of PAPI with rust ownership and wrapper of `PAPI_destroy_eventset` when `EventsSet`
/// is dropped.
impl Drop for EventsSet {
    fn drop(&mut self) {
        unsafe {
            PAPI_destroy_eventset(&mut self.evt_set_id);
        }
    }
}
