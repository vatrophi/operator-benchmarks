use std::time::Duration;

use crate::Machine;

/// Trait to implement your own metric based on hardware counters and duration.
pub trait MetricReporter {
    fn report(&self, counters_values: &[u64], duration: Duration) -> f64;
    fn name_with_unit(&self) -> &str;
}

/// One example of [MetricReporter] that compute the peak performance of a computation on a given
/// machine.
pub struct PeakPerfReporter {
    idx_cycles: usize,
    cycles_best: f64,
}

impl PeakPerfReporter {
    /// # Arguments
    /// `pb_fma`: number of fma (usefull or effectively done) to comple the computation.
    /// `dtype_size`: size of (in number of bytes) floating point numbers used in the computation.
    /// `threads`: number of threads used by the computation.
    ///
    /// # Error
    /// Panic if the given counters doesn't contains any CYCLES counters.
    pub fn new(
        machine: Machine,
        pb_fma: usize,
        dtype_size: usize,
        threads: usize,
        counters_names: &[&str],
    ) -> PeakPerfReporter {
        PeakPerfReporter {
            idx_cycles: counters_names
                .iter()
                .position(|&name| matches!(name, "CPU_CLK_UNHALTED" | "PAPI_TOT_CYC"))
                .expect("counters_names doesn't contains any cycles counter!"),
            cycles_best: machine.theoretical_cycles(
                pb_fma as u64,
                dtype_size as u64,
                threads as u64,
            ),
        }
    }
}

impl MetricReporter for PeakPerfReporter {
    fn report(&self, counters_values: &[u64], _duration: Duration) -> f64 {
        let cycles_real = counters_values[self.idx_cycles] as f64;
        self.cycles_best / cycles_real * 100.0
    }

    fn name_with_unit(&self) -> &str {
        "Peak Perf (%)"
    }
}

/// Another [MetricReporter] that implement a GFlops metric.
pub struct GflopsReporter {
    idx_ops: Vec<(usize, u64)>, // idx of counters (256B_PACKED_SINGLE)
                                // and n of flop i.e 256 / 32 = 8
}

impl GflopsReporter {
    pub fn new(counters_names: &[&str]) -> Self {
        let idx_ops: Vec<(usize, u64)> = counters_names
            .iter()
            .enumerate()
            .filter_map(|(i, name)| match *name {
                "FP_ARITH:SCALAR_SINGLE"
                | "FP_ARITH:SCALAR_DOUBLE"
                | "PAPI_SP_OPS"
                | "PAPI_DP_OPS" => Some((i, 1)),
                "FP_ARITH:128B_PACKED_SINGLE" => Some((i, 128 / 32)),
                "FP_ARITH:256B_PACKED_SINGLE" => Some((i, 256 / 32)),
                "FP_ARITH:512B_PACKED_SINGLE" => Some((i, 512 / 32)),
                "FP_ARITH:128B_PACKED_DOUBLE" => Some((i, 128 / 64)),
                "FP_ARITH:256B_PACKED_DOUBLE" => Some((i, 256 / 64)),
                "FP_ARITH:512B_PACKED_DOUBLE" => Some((i, 512 / 64)),
                _ => None,
            })
            .collect();
        if idx_ops.is_empty() {
            panic!("GflopsReporter cannot compute Gflop since no counter for flop as been given.");
        }

        if counters_names.contains(&"PAPI_SP_OPS") && idx_ops.len() > 1 {
            panic!("Choose 1 PAPI_SP_OPS (x)or 4 FP_ARITH counters but dont mix them.");
        }
        GflopsReporter { idx_ops }
    }
}

impl MetricReporter for GflopsReporter {
    fn report(&self, counters_values: &[u64], duration: Duration) -> f64 {
        let flop: u64 = self
            .idx_ops
            .iter()
            .map(|(i, nflop)| nflop * counters_values[*i])
            .sum();
        let g_flop = (flop as f64) / 1_000_000_000.0;
        g_flop / duration.as_secs_f64()
    }

    fn name_with_unit(&self) -> &str {
        "GFlops (e9/s)"
    }
}

/// A variant of [GflopsReporter] more simple used to filter some extra floating point operations
/// non needed for the entiere computation.
pub struct UsefulGFlops {
    ngflop: f64,
}

impl UsefulGFlops {
    /// It needs to get the number of effective expected flops
    pub fn new(nflop: usize) -> Self {
        UsefulGFlops {
            ngflop: nflop as f64 / 1_000_000_000.0,
        }
    }
}

impl MetricReporter for UsefulGFlops {
    fn report(&self, _counters_values: &[u64], duration: Duration) -> f64 {
        self.ngflop / duration.as_secs_f64()
    }

    fn name_with_unit(&self) -> &str {
        "Useful GFlops (e9/s)"
    }
}
