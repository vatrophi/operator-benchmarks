# operator-benchmarks

A library to help benchmarking some floating point operation intensive
code.

## Getting started

Simpy add this line to your `Cargo.toml` under the `[dependencies]` section.

```toml
operator-benchmarks = { version = "0.5", git = "https://gitlab.inria.fr/vatrophi/operator-benchmarks.git" }
```

If you get an error of cargo using git. You can create (or append this to) `.cargo/config.toml` containing this:

```toml
[net]
git-fetch-with-cli = true
```

Then simply:

- Write your computational intensive code or interface it by implementing the
  `Computation` trait.
  counters etc.
- Create a `Benchmark` using a default `Config` and run your `Computation`.

## A simple matmul example

```rust
use operator_benchmarks::*;
use serde::Serialize;

struct Matmul {
    n: usize,
    a: Vec<f32>,
    b: Vec<f32>,
    c: Vec<f32>,
}

impl Matmul {
    fn new(n: usize) -> Self {
        let flat_len = n * n;
        Matmul {
            n,
            a: (0..flat_len).map(|i| i as f32).collect(),
            b: (0..flat_len).map(|i| i as f32).collect(),
            c: vec![0.0;flat_len],
        }
    }
}

impl Computation for Matmul {
    fn name(&self) -> &str {
        "matmul"
    }
    fn execute(&mut self) {
        for i in 0..self.n {
            for j in 0..self.n {
                for k in 0..self.n {
                    self.c[i * self.n + j] += self.a[i * self.n + k] * self.b[k * self.n + j];
                }
            }
        }
    }
}

#[derive(Serialize)]
struct MetaDataMatmul {
    n: usize,
}

fn main() {
    let matmul = Matmul::new(100);
    let meta = MetaDataMatmul { n:100 };
    let res = Benchmark::new(Config::default()).run(matmul).result(meta);
    res.show_summary();
}
```

# Documentation

Simply build the documentation and open it in your browser with:

```bash
cargo doc --open
```

The main entry point is the `Benchmark` struct that you can use in a similar way of the example.
If you want to see what configuration option can be modified see the `Config` struct. You can create a default and modify it or simply use a config file in toml or any format that can be deserialize using [serde](https://serde.rs/).

Also one design goad is to lower the overhead of measurement that's why the `run` method of `Benchmark` is generic and don't use some `Box<dyn Computation>` who could result in easier to use code for the user of library. This idea might be bad and the design might evolve to also support dyn trait objects.

## Notes for versionning

This crate is not published to [crates.io](https://crates.io) but we are using some version numbers to avoid breaking easily the code of experiment. So for changes that will not change function name or signatures or delete a `pub fn`, simply increment the 3rd version number in `Cargo.toml`. And increment the other ones in case of breaking changes.
