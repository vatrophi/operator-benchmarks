use std::fs::{read_to_string, File};
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::Command;

fn main() {
    let out_dir = PathBuf::from(std::env::var_os("OUT_DIR").unwrap());
    println!("cargo:rerun-if-changed=build.rs");

    // bindgen for papi
    println!("cargo:rustc-link-lib=papi");
    println!("cargo:rerun-if-changed=papi_wrapper.h");
    bindgen::Builder::default()
        .header("papi_wrapper.h")
        .allowlist_var("PAPI_(VER_CURRENT|OK|NULL)")
        .allowlist_function("PAPI_(library_init|shutdown|event_name_to_code|strerror|start|stop|create_eventset|add_event|destroy_eventset)")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings")
        .write_to_file(out_dir.join("papi_bindings.rs"))
        .expect("Couldn't write bindings");

    // write current_machine to the default trait of Machine struct
    let path = out_dir.join("current_machine.rs");
    let mut file = File::create(path).unwrap();
    let (name, fma, vecsize) = run_script();

    write!(
        file,
        r#"
        use super::Machine;
        pub fn machine() -> Machine {{
            Machine {{ 
                name: String::from("{name}"),
                fma: {fma}, 
                cores: {ncores},
                vector_size: {vecsize}, 
                l1_cache_size:{l1},
                l2_cache_size:{l2},
                l3_cache_size:{l3},
            }}
        }}
    "#,
        ncores = num_cpus::get_physical(),
        l1 = cache_size::l1_cache_size().unwrap(),
        l2 = cache_size::l2_cache_size().unwrap(),
        l3 = cache_size::l3_cache_size().unwrap(),
    )
    .unwrap();
}

fn hostname() -> String {
    read_to_string("/etc/hostname").unwrap().trim().to_owned()
}

const NO_TURBO_PATH: &str = "/sys/devices/system/cpu/intel_pstate/no_turbo";
const NO_PARANO_PATH: &str = "/proc/sys/kernel/perf_event_paranoid";
const NO_PARANO_LIMIT: i32 = 2;

fn is_turbo_disabled() -> bool {
    read_to_string(NO_TURBO_PATH).unwrap().trim() == "1"
}

fn is_paranoid_disabled() -> bool {
    read_to_string(NO_PARANO_PATH)
        .unwrap()
        .trim()
        .parse::<i32>()
        .unwrap()
        < NO_PARANO_LIMIT
}

fn run_script() -> (String, u64, u64) {
    let path = Path::new("perf-fma");
    let (mtune, march, srcfile) = if is_x86_feature_detected!("avx512f") {
        (
            "-mtune=skylake-avx512",
            "-march=skylake-avx512",
            "perf_avx512.c",
        )
    } else if is_x86_feature_detected!("avx2") {
        ("-mtune=core-avx2", "-march=core-avx2", "perf_avx2.c")
    } else {
        return (hostname(), 1, 4);
    };
    let path = path;
    let compile_ok = Command::new("gcc")
        .args([
            "-O2",
            "-Wall",
            mtune,
            march,
            path.join(srcfile).to_str().unwrap(),
            "--save-temps",
            "-o",
            path.join("perf_script").to_str().unwrap(),
        ])
        .status()
        .unwrap()
        .success();
    assert!(compile_ok);
    if !is_turbo_disabled() {
        panic!("Turbo boost is enabled! To disable it try `sudo sh -c 'echo 1 > {NO_TURBO_PATH}'`");
    }

    if !is_paranoid_disabled() {
        panic!("Perf event paranoid is enabled! To disable it try `sudo sh -c 'echo 1 > {NO_PARANO_PATH}'`");
    }
    // set perfscript to execute on the CPU  0
    let perf_out = Command::new("taskset")
        .args(["0x1", path.join("perf_script").to_str().unwrap()])
        .output()
        .unwrap()
        .stdout;
    let perf_out = String::from_utf8(perf_out).unwrap();
    let (mut vecsize, mut fma) = (None, None);
    for line in perf_out.lines() {
        if line.starts_with("vector_size") {
            vecsize = line
                .split(' ')
                .last()
                .map(|d| 4 * d.parse::<u64>().unwrap());
            // size is in number of floats and 1float = 4bytes
        }
        if line.starts_with("n_fma") {
            fma = line
                .split(' ')
                .last()
                .map(|f| f.parse::<f32>().unwrap().round() as u64);
        }
    }
    (hostname(), fma.unwrap(), vecsize.unwrap())
}
